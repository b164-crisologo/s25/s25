//Number 2
db.fruits.aggregate([
    {$unwind: "$origin"},
    { $group: {_id: "$origin", fruitsPerCountry: {$sum: 1} } },
])



//Number 3

db.fruits.aggregate([
    { $match: { onSale: true } },
    { $count: "FruitsOnSale" }
])



//Number 4
db.fruits.aggregate([
	{$match: {stock: {$gte: 20}}},
	{$count: "enoughStock"}
])


//Number 5
    db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", avg_price: {$avg: "price"}}}
])

//number 6

    db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", avg_price: {$max: "price"}}}
])

//number 7

    db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", avg_price: {$min: "price"}}}
])